package com.quintaos.pathfinder.views.adapters

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.quintaos.pathfinder.R

class IntroPagerAdapter(private val context: Context, private val layouts: IntArray,
                        private val btn_skip: Button, private val btn_next: Button,
                        private val ll_dots: LinearLayout) : PagerAdapter(), ViewPager.OnPageChangeListener {

    private var layoutInflater: LayoutInflater? = null

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        val view = layoutInflater!!.inflate(layouts[position], container, false)
        container.addView(view)

        return view
    }

    override fun getCount(): Int {
        return layouts.size
    }

    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view === obj
    }

    override fun destroyItem(container: ViewGroup, position: Int, any: Any) {
        val view = any as View
        container.removeView(view)
    }

    override fun onPageScrollStateChanged(state: Int) {
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
        addBottomDots(position)

        // changing the next button text 'NEXT' / 'GOT IT'
        if (position == layouts.size - 1) {
            // last page. make button text to GOT IT
            btn_next.text = "Start"
            btn_skip.visibility = View.GONE
        } else {
            // still pages are left
            btn_next.text = "Next"
            btn_skip.visibility = View.VISIBLE
        }
    }

    fun addBottomDots(currentPage: Int) {
        val dots = arrayOfNulls<TextView>(layouts.size)
        val colorsActive = context.resources.getIntArray(R.array.array_dot_active)
        val colorsInactive = context.resources.getIntArray(R.array.array_dot_inactive)

        ll_dots.removeAllViews()
        for (i in 0 until dots.size) {
            dots[i] = TextView(context)
            dots[i]!!.text = "•"
            dots[i]!!.textSize = 35f
            dots[i]!!.setTextColor(colorsInactive[currentPage])
            ll_dots.addView(dots[i])
        }

        if (dots.isNotEmpty())
            dots[currentPage]!!.setTextColor(colorsActive[currentPage])
    }
}