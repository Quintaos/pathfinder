package com.quintaos.pathfinder

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.WindowManager

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.dot_dark_screen3)

        setContentView(R.layout.activity_login)

        val handler = Handler()
        handler.postDelayed({
            startActivity(Intent(this@LoginActivity, MainActivity::class.java))
            finish()
        }, 5000)   //5 seconds
    }

}
