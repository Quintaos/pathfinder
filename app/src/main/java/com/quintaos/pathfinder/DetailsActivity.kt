package com.quintaos.pathfinder

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import com.quintaos.pathfinder.models.Place
import com.quintaos.pathfinder.utils.BitmapLoader
import kotlinx.android.synthetic.main.activity_details.*

class DetailsActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        setSupportActionBar(toolbar)

        val mItem = intent.getParcelableExtra<Place>("mPlace")

        iv_placePic2.setImageBitmap(BitmapLoader.decodeSampledBitmapFromResource(resources, mItem.picture, 70, 70))
        tv_placeName2.text = mItem.name
        tv_placeInfo2.text = mItem.info
        tv_openHours.text = mItem.openFromTo


        var fabText = "A ticket has been purchased"

        if (mItem.isHotel) {
            fab.setImageResource(R.drawable.ic_booking)
            fabText = "Your hotel has been booked"
        }

        fab.setOnClickListener { view ->
            Snackbar.make(view, fabText, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }

        toolbar_layout.setOnClickListener {
            this.onBackPressed()
        }

    }

}
