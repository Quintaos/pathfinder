package com.quintaos.pathfinder

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.WindowManager
import android.widget.Button
import com.quintaos.pathfinder.utils.ParallaxPageTransformer
import com.quintaos.pathfinder.utils.ParallaxPageTransformer.ParallaxTransformInformation
import com.quintaos.pathfinder.views.adapters.IntroPagerAdapter
import kotlinx.android.synthetic.main.activity_intro.*


class IntroActivity : AppCompatActivity() {

    // layouts of all welcome sliders
    private var layouts = intArrayOf(R.layout.slide_1, R.layout.slide_2, R.layout.slide_3)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intro)

        // making notification bar transparent
        changeStatusBarColor()

        val introViewPager = findViewById<ViewPager>(R.id.introViewPager)
        val mIntroPagerAdapter = IntroPagerAdapter(this, layouts, btn_skip, btn_next, ll_dots)
        val btnSkip = findViewById<Button>(R.id.btn_skip)
        val btnNext = findViewById<Button>(R.id.btn_next)

        mIntroPagerAdapter.addBottomDots(0)
        introViewPager.adapter = mIntroPagerAdapter
        introViewPager.addOnPageChangeListener(mIntroPagerAdapter)

        val pageTransformer = ParallaxPageTransformer()
                .addViewToParallax(ParallaxTransformInformation(R.id.introImage, 0.6f, 0.4f))
                .addViewToParallax(ParallaxTransformInformation(R.id.smallText, 0.8f, 0.6f))
                .addViewToParallax(ParallaxTransformInformation(R.id.bigText, 1f, 0.8f))

        introViewPager.setPageTransformer(true, pageTransformer)

        btnSkip.setOnClickListener {
            launchLoginScreen()
        }

        btnNext.setOnClickListener {
            // checking for last page
            // if last page home screen will be launched
            val current = getItem(+1)
            if (current < layouts.size) {
                // move to next screen
                introViewPager.currentItem = current
            } else {
                launchLoginScreen()
            }
        }
    }

    private fun getItem(i: Int): Int {
        return introViewPager.currentItem + i
    }

    private fun launchLoginScreen() {
        startActivity(Intent(this@IntroActivity, LoginActivity::class.java))
        finish()
    }

    /**
     * Making notification bar transparent
     */
    private fun changeStatusBarColor() {
        val window = window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = Color.TRANSPARENT
    }
}
