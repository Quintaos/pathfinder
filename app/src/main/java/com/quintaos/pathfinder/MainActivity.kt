package com.quintaos.pathfinder

import android.animation.ArgbEvaluator
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.design.widget.TabLayout
import android.support.v4.app.ActivityCompat
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import com.quintaos.pathfinder.fragments.PageFragment
import com.quintaos.pathfinder.models.Place
import com.quintaos.pathfinder.utils.ParallaxPageTransformer
import com.quintaos.pathfinder.utils.ParallaxPageTransformer.ParallaxTransformInformation
import com.quintaos.pathfinder.views.adapters.CardListAdapter
import com.quintaos.pathfinder.views.adapters.DestinationsPagerAdapter
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity(), CardListAdapter.OnListFragmentInteractionListener {

    private var mDestinationsPagerAdapter: DestinationsPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val mViewPager = findViewById<ViewPager>(R.id.container)
        val mTabLayout = findViewById<TabLayout>(R.id.tabs)
        val appBarLayout = findViewById<AppBarLayout>(R.id.appBarLayout)
        val appBarLayoutExtension = findViewById<View>(R.id.appBarLayoutExtension)

        // initiate Pager Adapter
        mDestinationsPagerAdapter = DestinationsPagerAdapter(supportFragmentManager)

        val romeList = ArrayList<Place>()
        romeList.add(Place("Colosseum", getString(R.string.large_text), "09:00 to 17:00", R.drawable.colosseum, false))
        romeList.add(Place("Pantheon", getString(R.string.large_text), "09:00 to 17:00", R.drawable.pantheon, false))
        romeList.add(Place("Hotel", getString(R.string.large_text), "24/7", R.drawable.hotel, true))

        val parisList = ArrayList<Place>()
        parisList.add(Place("Eiffel Tower", getString(R.string.large_text), "09:00 to 17:00", R.drawable.eiffel, false))
        parisList.add(Place("Louvre", getString(R.string.large_text), "09:00 to 17:00", R.drawable.louvre, false))
        parisList.add(Place("Hotel", getString(R.string.large_text), "24/7", R.drawable.hotel, true))

        val barcaList = ArrayList<Place>()
        barcaList.add(Place("Park Güell", getString(R.string.large_text), "09:00 to 17:00", R.drawable.guell, false))
        barcaList.add(Place("Sagrada Família", getString(R.string.large_text), "09:00 to 17:00", R.drawable.sagrada, false))
        barcaList.add(Place("Hotel", getString(R.string.large_text), "24/7", R.drawable.hotel, true))


        // add pages to the adapter
        mDestinationsPagerAdapter!!.addFrag(PageFragment.newInstance(romeList))
        mDestinationsPagerAdapter!!.addFrag(PageFragment.newInstance(parisList))
        mDestinationsPagerAdapter!!.addFrag(PageFragment.newInstance(barcaList))

        // set adapter to viewPager
        mViewPager.adapter = mDestinationsPagerAdapter
        mViewPager.offscreenPageLimit = 3
        mTabLayout.setupWithViewPager(mViewPager)


        val pageTransformer = ParallaxPageTransformer()
                .addViewToParallax(ParallaxTransformInformation(R.id.mRecyclerView, -1f, -1f))

        mViewPager.setPageTransformer(true, pageTransformer)


        mViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                val tabColors = resources.getIntArray(R.array.array_page_theme)
                val tabDarkColors = resources.getIntArray(R.array.array_dot_inactive)
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)

                if (position < mDestinationsPagerAdapter!!.count - 1 && position < tabColors.size - 1) {
                    appBarLayout.setBackgroundColor(ArgbEvaluator().evaluate(positionOffset, tabColors[position], tabColors[position + 1]) as Int)
                    appBarLayoutExtension.setBackgroundColor(ArgbEvaluator().evaluate(positionOffset, tabColors[position], tabColors[position + 1]) as Int)
                    mTabLayout.setSelectedTabIndicatorColor(ArgbEvaluator().evaluate(positionOffset, tabDarkColors[position], tabDarkColors[position + 1]) as Int)
                    window.statusBarColor = (ArgbEvaluator().evaluate(positionOffset, tabDarkColors[position], tabDarkColors[position + 1]) as Int)
                } else {
                    appBarLayout.setBackgroundColor(tabColors[tabColors.size - 1])
                    appBarLayoutExtension.setBackgroundColor(tabColors[tabColors.size - 1])
                    tabs.setSelectedTabIndicatorColor(tabDarkColors[tabDarkColors.size - 1])
                    window.statusBarColor = (tabDarkColors[tabDarkColors.size - 1])
                }
            }

            override fun onPageSelected(position: Int) {
            }

            override fun onPageScrollStateChanged(state: Int) {
            }
        })
    }

    override fun onListFragmentInteraction(placePic: ImageView, placeName: TextView, placeInfo: TextView, mItem: Place) {
        val intent = Intent(this@MainActivity, DetailsActivity::class.java)
        intent.putExtra("mPlace", mItem)

        val pair1 = android.support.v4.util.Pair<View, String>(placePic as View, placePic.transitionName)
        val pair2 = android.support.v4.util.Pair<View, String>(placeName as View, placeName.transitionName)
        val pair3 = android.support.v4.util.Pair<View, String>(placeInfo as View, placeInfo.transitionName)

        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this@MainActivity, pair1, pair2, pair3)
        ActivityCompat.startActivity(this@MainActivity, intent, options.toBundle())

    }
}
