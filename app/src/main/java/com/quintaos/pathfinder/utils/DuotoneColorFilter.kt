package com.quintaos.pathfinder.utils

import android.graphics.Color
import android.graphics.ColorFilter
import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.support.annotation.ColorInt

fun DuotoneColorFilter(@ColorInt colorBlack: Int, @ColorInt colorWhite: Int, contrast: Float): ColorFilter {
    val cm = ColorMatrix()

    val cmBlackWhite = ColorMatrix()
    val lumR = 0.2125f
    val lumG = 0.7154f
    val lumB = 0.0721f
    val blackWhiteArray = floatArrayOf(lumR, lumG, lumB, 0f, 0f, lumR, lumG, lumB, 0f, 0f, lumR, lumG, lumB, 0f, 0f, 0f, 0f, 0f, 1f, 0f)
    cmBlackWhite.set(blackWhiteArray)

    val cmContrast = ColorMatrix()
    val scale = contrast + 1.0f
    val translate = (-0.5f * scale + 0.5f) * 255f
    val contrastArray = floatArrayOf(scale, 0f, 0f, 0f, translate, 0f, scale, 0f, 0f, translate, 0f, 0f, scale, 0f, translate, 0f, 0f, 0f, 1f, 0f)
    cmContrast.set(contrastArray)

    val cmDuoTone = ColorMatrix()
    val r1 = Color.red(colorWhite)
    val g1 = Color.green(colorWhite)
    val b1 = Color.blue(colorWhite)
    val r2 = Color.red(colorBlack)
    val g2 = Color.green(colorBlack)
    val b2 = Color.blue(colorBlack)
    val r1r2 = (r1 - r2) / 255f
    val g1g2 = (g1 - g2) / 255f
    val b1b2 = (b1 - b2) / 255f
    val duoToneArray = floatArrayOf(r1r2, 0f, 0f, 0f, r2.toFloat(), g1g2, 0f, 0f, 0f, g2.toFloat(), b1b2, 0f, 0f, 0f, b2.toFloat(), 0f, 0f, 0f, 1f, 0f)
    cmDuoTone.set(duoToneArray)

    cm.postConcat(cmBlackWhite)
    cm.postConcat(cmContrast)
    cm.postConcat(cmDuoTone)

    return ColorMatrixColorFilter(cm)
}