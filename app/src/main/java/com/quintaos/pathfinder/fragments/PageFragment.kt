package com.quintaos.pathfinder.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.quintaos.pathfinder.R
import com.quintaos.pathfinder.models.Place
import com.quintaos.pathfinder.views.adapters.CardListAdapter
import java.util.*

class PageFragment : Fragment() {

    private var placeList: ArrayList<Place>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            placeList = arguments.getParcelableArrayList<Place>(PLACE_LIST)
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view: View = inflater!!.inflate(R.layout.fragment_page, container, false)
        val mRecyclerView = view.findViewById<RecyclerView>(R.id.mRecyclerView)

        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.setItemViewCacheSize(2)
        mRecyclerView.isDrawingCacheEnabled = true
        mRecyclerView.drawingCacheQuality = View.DRAWING_CACHE_QUALITY_HIGH

        mRecyclerView.layoutManager = LinearLayoutManager(activity)
        mRecyclerView.adapter = CardListAdapter(context, placeList)

        return view
    }

    companion object {
        private const val PLACE_LIST = "placeList"

        fun newInstance(list: ArrayList<Place>): PageFragment {
            val fragment = PageFragment()
            val args = Bundle()
            args.putParcelableArrayList(PLACE_LIST, list)
            fragment.arguments = args
            return fragment
        }
    }
}
