package com.quintaos.pathfinder.views.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.quintaos.pathfinder.R
import com.quintaos.pathfinder.models.Place
import com.quintaos.pathfinder.utils.BitmapLoader
import java.util.*


class CardListAdapter(private val context: Context, private val list: ArrayList<Place>?) : RecyclerView.Adapter<CardListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent!!.context).inflate(R.layout.cardview, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val mItem = list!![position]
        holder.placePic.setImageBitmap(BitmapLoader.decodeSampledBitmapFromResource(context.resources, mItem.picture, 10, 10))
        holder.placeName.text = mItem.name
        holder.placeInfo.text = mItem.info

        holder.mView.setOnClickListener {

            holder.placePic.transitionName = "placePic"
            holder.placeName.transitionName = "placeName"
            holder.placeInfo.transitionName = "placeInfo"

            val listener = context as OnListFragmentInteractionListener
            listener.onListFragmentInteraction(holder.placePic, holder.placeName, holder.placeInfo, mItem)
        }
    }

    override fun getItemCount(): Int {
        return list!!.size
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val placePic: ImageView = mView.findViewById(R.id.iv_placePic)
        val placeName: TextView = mView.findViewById(R.id.tv_placeName)
        val placeInfo: TextView = mView.findViewById(R.id.tv_placeInfo)
    }

    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(placePic: ImageView, placeName: TextView, placeInfo: TextView, mItem: Place)
    }

}