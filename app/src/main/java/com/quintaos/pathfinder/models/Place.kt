package com.quintaos.pathfinder.models

import android.os.Parcel
import android.os.Parcelable

class Place( var name: String,  var info: String,  var openFromTo: String,  var picture: Int,  var isHotel: Boolean) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readByte() != 0.toByte())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(info)
        parcel.writeString(openFromTo)
        parcel.writeInt(picture)
        parcel.writeByte(if (isHotel) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Place> {
        override fun createFromParcel(parcel: Parcel): Place {
            return Place(parcel)
        }

        override fun newArray(size: Int): Array<Place?> {
            return arrayOfNulls(size)
        }
    }


}
